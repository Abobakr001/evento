import 'package:http/http.dart';
import 'events.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';

class Helper {
  Client _client;

  Helper() {
    this._client = Client();
  }

  Future<List<Event>> getAllManga(String period) async {
    period = period.replaceAll("-", "_");
    List<String> info = period.split("_");

    if (info[4].toString().length == 1) {
      info[4] = "0" + info[4].toString();
    }
    if (info[5].toString().length == 1) {
      info[4] = "0" + info[5].toString();
    }

    final todayDate = DateTime.now();
    String dur = info[3].toString() +
        info[4].toString() +
        info[5].toString() +
        'T' +
        "000000";

    DateTime duration = DateTime.parse(dur);
    final difference = duration.difference(todayDate).inDays;
    print(difference);

    String endDate = "";
    if (difference <= 30) {
      endDate = duration.toString();
    }
    if (difference >= 30 && difference < 60) {
      endDate = todayDate.add(new Duration(days: 60)).toString();
    }
    if (difference >= 60 && difference < 90) {
      endDate = todayDate.add(new Duration(days: 90)).toString();
    }
    if (difference >= 90 && difference < 180) {
      endDate = todayDate.add(new Duration(days: 180)).toString();
    }
    if (difference >= 180) {
      endDate = todayDate.add(new Duration(days: 365)).toString();
    }

    endDate = endDate.split(" ")[0];
    endDate = endDate.replaceAll("-", "_");
    String durationPeriod = info[0].toString() +
        "_" +
        info[1].toString() +
        "_" +
        info[2].toString() +
        "-" +
        endDate.toString();

    List<Event> allManga = [];
    String title = "";
    String description = "";
    final response = await _client.get(
        'https://www.debrecen.hu/en/tourist/events/filter/date-' +
            durationPeriod); //2020_4_29-2020_4_29');
    final document = parse(response.body);
    final mangasPerTitle = document.getElementsByClassName('itembox event');
    for (Element mangaPerTitle in mangasPerTitle) {
      final url = mangaPerTitle.attributes['href'];
      print(url);
      final titleClass =
          mangaPerTitle.getElementsByClassName('itembox-title color1');
      for (Element m in titleClass) {
        title = m.text;
        title = title
            .trimRight()
            .trimLeft(); // replaceAll(new RegExp(r"\s\b|\b\s"), "");
        print(title);
      }
      print(titleClass);
      final descriptionClass =
          mangaPerTitle.getElementsByClassName('itembox-copy');
      for (Element m in descriptionClass) {
        description = m.text;
        description = description
            .trimRight()
            .trimLeft(); //.replaceAll(new RegExp(r"\s\b|\b\s"), "");
        print(description);
      }

      final manga = Event(title: title, description: description, url: url);
      allManga.add(manga);
    }
    return allManga;
  }
}
