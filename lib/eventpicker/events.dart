class Event {
  String title;
  String description;
  String url;

  Event({this.title, this.description, this.url});
}