import 'package:flutter/material.dart';
import 'helper.dart';
import 'events.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(new MaterialApp(home: new MyTextInput()));
}

class MyTextInput extends StatefulWidget {
  @override
  MyTextInputState createState() => new MyTextInputState();
}

class MyTextInputState extends State<MyTextInput> {
  DateTime dateTimeFrom;
  DateTime dateTimeTo;
  String result;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Home Page"),
        ),
        body: new ListView(
          children: <Widget>[
            new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    dateTimeFrom == null
                        ? "Nothing has been picked yet"
                        : dateTimeFrom.year.toString() +
                            "-" +
                            dateTimeFrom.month.toString() +
                            "-" +
                            dateTimeFrom.day.toString(),
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 21,
                        fontWeight: FontWeight.w900),
                  ),
                  RaisedButton(
                    child: Text("Pick a date - From"),
                    onPressed: () {
                      showDatePicker(
                              context: context,
                              initialDate:
                                  DateTime.now().add(Duration(days: 1)),
                              firstDate: DateTime.now().add(Duration(days: 0)),
                              lastDate: DateTime(DateTime.now().year + 5))
                          .then((date) {
                        setState(() {
                          dateTimeFrom = date;
                        });
                      });
                    },
                  ),
                ]),
            new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    dateTimeTo == null
                        ? "Nothing has been picked yet"
                        : dateTimeTo.year.toString() +
                            "-" +
                            dateTimeTo.month.toString() +
                            "-" +
                            dateTimeTo.day.toString(),
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 21,
                        fontWeight: FontWeight.w900),
                  ),
                  RaisedButton(
                    child: Text("Pick a date - To"),
                    onPressed: () {
                      showDatePicker(
                              context: context,
                              initialDate:
                                  DateTime.now().add(Duration(days: 30)),
                              firstDate: DateTime.now().add(Duration(days: 29)),
                              lastDate: DateTime(DateTime.now().year + 5))
                          .then((date) {
                        setState(() {
                          dateTimeTo = date;
                        });
                      });
                    },
                  ),
                ]),
            new ListTile(
              title: new RaisedButton(
                child: new Text("Next"),
                onPressed: () {
                  var route;
                  if (dateTimeFrom != null && dateTimeTo != null) {
                    route = new MaterialPageRoute(
                      builder: (BuildContext context) => new NextPage(
                        period: (dateTimeFrom.year.toString() +
                            "_" +
                            dateTimeFrom.month.toString() +
                            "_" +
                            dateTimeFrom.day.toString() +
                            "-" +
                            dateTimeTo.year.toString() +
                            "_" +
                            dateTimeTo.month.toString() +
                            "_" +
                            dateTimeTo.day.toString()),
                      ),
                    );
                    Navigator.of(context).push(route);
                  } else {
                    showDialog(
                      context: context,
                      child: Center(
                          child: new AlertDialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(25)),
//                          title: new Text(""),
                        content: new Text("Forgot to pick date ... ",
                            style: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold,
                                fontSize: 20)),
                        backgroundColor: Color.fromARGB(220, 117, 218, 255),
                      )),
                    );
                  }
                },
              ),
            ),
          ],
        ));
  }
}

class NextPage extends StatefulWidget {
  @override
  MyHomePageState createState() => MyHomePageState();

  final String period;

  NextPage({Key key, this.period}) : super(key: key);
}

class MyHomePageState extends State<NextPage> {
  Helper _helper = Helper();

  @override
  Widget build(BuildContext context) {
//    print(widget.period);
    return Scaffold(
      appBar: AppBar(
        title: Text("List of events"),
      ),
      //body: new Text("${widget.period}"),
      body: FutureBuilder<List<Event>>(
        initialData: List<Event>(),
        future: _helper.getAllManga(widget.period),
        builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Center(
                child: RefreshProgressIndicator(),
              );
            case ConnectionState.none:
              return Center(
                child: Text('Tidak ada koneksi'),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Center(
                  child: Text('Data yang diterima salah'),
                );
              }
              return ListView.separated(
                itemCount: snapshot.data.length,
                separatorBuilder: (BuildContext context, int index) =>
                    Divider(thickness: 2),
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            snapshot.data[index].title,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            snapshot.data[index].description,
                            style:
                                TextStyle(color: Colors.black, fontSize: 14.0),
                          ),
                        ]),
                    onTap: () async {
                      if (await canLaunch(snapshot.data[index].url)) {
                        await launch(snapshot.data[index].url);
                      }
                    },
                  );
                },
              );
          }
          return null;
        },
      ),
    );
  }
}
